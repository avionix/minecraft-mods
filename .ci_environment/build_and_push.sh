#! /bin/bash

# docker build -t avionixg/packwiz:latest .
#docker image push avionixg/packwiz:latest

# Multiarchitecture build
docker buildx use multiarch
docker buildx build --platform linux/amd64,linux/arm64 -t avionixg/packwiz:latest --push .
