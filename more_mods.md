# leaves-us-in-peace
https://modrinth.com/mod/leaves-us-in-peace

# cleardespawn
Makes items blink as they're about to despawn.

# spice-of-fabric
Spice of life hunger

# aether
The aether!

# cammies-wearable-backpacks
Backpacks that take up an armor slot.

# cammie's hookshot
https://modrinth.com/mod/hookshot

# snails
Snails.

# mining-master
Add new ores to use for enchanting

# easiercrafting
Shows a list of things you can craft

# chest-cavity
Collect and rearrange internal organs.

# immersiveportals / immersivecursedness
Seamless portals

# immersive-weathering
Allow you to selectively weather bricks

# charm
A variety of improvements and additions to vanilla mechanics

# charmonium
Client side SFX and background music additions

# woodcutter-compats
(support for other mods) Updates woodcutter to work with other wood mods

# dashloader
Cache startup files

# plasmo-voice
Proximity voice chat

# neutrino
https://modrinth.com/mod/neutrino

# paradise-mod
https://modrinth.com/mod/paradise-mod

# spice of fabric
Food is the spice of life.

# resounding
Ray tracing sound mod
https://modrinth.com/mod/resounding

# actionhunger
Hunger overhaul

# not enough crashes
Prettier crash reports

# right click harvest
https://modrinth.com/mod/right-click-harvest

# roughly enough items
Comparable to JEI.

# seamless loading screen
Recently updated. https://modrinth.com/mod/seamless-loading-screen

# infinity water bucket
https://modrinth.com/mod/infinity-water-bucket

# snorkel
https://modrinth.com/mod/snorkel

# culinaire
More foods

# boat-item-view
See maps while moving in a boat.
https://modrinth.com/mod/boat-item-view

# aphid-additions
Adds a rideable aphid to the End.

# enderscape
https://modrinth.com/mod/enderscape

# friends and foes
https://modrinth.com/mod/friends-and-foes

