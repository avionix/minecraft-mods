## Artifact URLs

A CI job produces .zip archives for client and server versions of the modpack (to save on network usage).

Example for the UEG server modpack:
https://gitlab.com/avionix/minecraft-mods/-/jobs/artifacts/master/raw/ueg-modpack-server.zip?job=ueg

Set path/filename and job title.

[More details on artifacts.](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)

Because Gitlab CI doesn't support loops, new modpacks need to have the job manually copy-pasted.
Set the job title to the directory name (which is also the pack title) of the modpack.

Example MultiMC start command for packwiz:
`"$INST_JAVA" -jar packwiz-installer-bootstrap.jar https://gitlab.com/avionix/minecraft-mods/-/raw/master/ueg/pack.toml`
